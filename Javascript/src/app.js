const { get_total_amount, get_shipping_gift_amount } = require('./utils/amount');
const get_products_input_user = require('./utils/input');
const init_products = require('./utils/initialize');
const print_all_info = require('./utils/print');
const get_best_discount = require('./utils/discount');

function main() {
    let products = init_products();
    products = get_products_input_user(products);
    
    const total_amount = get_total_amount(products);
    const discount_info = get_best_discount(products, total_amount);
    const shipping_gift_amount = get_shipping_gift_amount(products);
    
    print_all_info(products, total_amount, discount_info, shipping_gift_amount);
}

main();