module.exports = {
    QUANTITY_PER_PACKAGE: 10,
    PRICE_PER_PACKAGE: 5,
    TRUE_LOWERCASE: 'true',
    FALSE_LOWERCASE: 'false'
};