module.exports = {
    DISCOUNT_NAME: 'discount_name',
    DISCOUNT_AMOUNT: 'discount_amount',
    SHIPPING_AMOUNT: 'shipping_amount',
    GIFT_AMOUNT: 'gift_amount'
};