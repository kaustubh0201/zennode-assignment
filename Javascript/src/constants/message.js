function quantity_input(product_name, product_price) {
    return `How much amount of ${product_name} is needed with each unit price as \$${product_price} ? \n`;
}

module.exports = {
    GIFT_WRAPPER_INPUT_MESSAGE: 'Do you want the gift wrapped ? (Please write True or False): \n',
    PRODUCT_QUANTITY_INPUT_MESSAGE: quantity_input,
    NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE: 'Only non-negative integers are accepted as input.',
    NEGATIVE_QUANTITY_ERROR_MESSAGE: 'Got negative input for quantity.',
    INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE: 'Got invalid input for gift wrapper.',
    INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE: 'Got invalid input for product quantity.'
};