// sign for cash
const DOLLAR = "$"

// print product info variables
const PRODUCT_NAME = "Product Name: "
const PRODUCT_QUANTITY = "Product Quantity: "
const PRODUCT_TOTAL_AMOUNT = "Product Total Amount: "

// print sub total variables
const SUB_TOTAL = "Sub Total: "

// print discount info variables
const APPLIED_DISCOUNT_NAME = "Applied Discount Name: "
const APPLIED_DISCOUNT_AMOUNT = "Applied Discount Amount: "

// print the miscellaneous charges variables
const SHIPPING_FEE = "Shipping Fee: "
const GIFT_WRAP_FEE = "Gift Wrap Fee: "

// print the total variable
const TOTAL = "Total: "

function print_product_info_message(product_name, product_quantity, product_total_amount) {
    return `\n\n${PRODUCT_NAME}${product_name}\n${PRODUCT_QUANTITY}${product_quantity}\n${PRODUCT_TOTAL_AMOUNT}${DOLLAR}${product_total_amount}`;
}

function print_subtotal(subtotal) {
    return `\n\n${SUB_TOTAL}${DOLLAR}${subtotal}`;
}

function print_discount_info(discount_name, discount_amount) {
    return `\n\n${APPLIED_DISCOUNT_NAME}${discount_name}\n${APPLIED_DISCOUNT_AMOUNT}${DOLLAR}${discount_amount}`;
}

function print_shipping_gift_amount(shipping_fee, gift_wrap_fee) {
    return `\n\n${SHIPPING_FEE}${DOLLAR}${shipping_fee}\n${GIFT_WRAP_FEE}${DOLLAR}${gift_wrap_fee}`;
}

function print_total(total) {
    return `\n\n${TOTAL}${DOLLAR}${total}`;
}

module.exports = {
    PRINT_PRODUCT_INFO_MESSAGE: print_product_info_message,
    PRINT_SUBTOTAL: print_subtotal,
    PRINT_DISCOUNT_INFO: print_discount_info,
    PRINT_SHIPPING_GIFT_AMOUNT: print_shipping_gift_amount,
    PRINT_TOTAL: print_total
};