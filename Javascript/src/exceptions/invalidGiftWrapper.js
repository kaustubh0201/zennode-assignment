const { INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE } = require('../constants/message');

class InvalidGiftWrapperInputException extends Error {
    constructor(message = INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE) {
        super(message);
        this.name = 'InvalidGiftWrapperInputException';
    }
};

module.exports = InvalidGiftWrapperInputException;