const { INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE } = require('../constants/message');

class InvalidInputForProductQuantity extends Error {
    constructor(message = INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE) {
        super(message);
        this.name = 'InvalidInputForProductQuantity';
    }
};

module.exports = InvalidInputForProductQuantity;