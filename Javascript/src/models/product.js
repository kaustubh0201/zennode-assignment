class Product {
    constructor(product_name, product_price) {
        this.name = product_name
        this.price = product_price
        this.quantity = 0
        this.isWrapped = false
    }
};

module.exports = Product;