const { QUANTITY_PER_PACKAGE, PRICE_PER_PACKAGE} = require('../constants/constant')
const { SHIPPING_AMOUNT, GIFT_AMOUNT } = require('../constants/dictionary')

function get_total_amount(products) {
    total_amount = 0;

    products.forEach((product) => {
        total_amount += (product.quantity * product.price);
    });

    return total_amount;
};

function get_shipping_gift_amount(products) {
    shipping_amount = 0;
    gift_wrap_amount = 0;

    products.forEach((product) => {
        if (product.isWrapped == true) {
            gift_wrap_amount += product.quantity;
        }

        shipping_amount += Math.ceil(product.quantity / QUANTITY_PER_PACKAGE) * PRICE_PER_PACKAGE;
    });

    return {
        [SHIPPING_AMOUNT]: shipping_amount,
        [GIFT_AMOUNT]: gift_wrap_amount
    }
};

module.exports = {
    get_total_amount,
    get_shipping_gift_amount
};