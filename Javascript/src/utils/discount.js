const { EMPTY_STRING, EMPTY_LIST } = require('../constants/empty');
const {
    DISCOUNT_NAME,
    DISCOUNT_AMOUNT
} = require('../constants/dictionary');
const {
    FLAT_10_DISCOUNT,
    FLAT_10_DISCOUNT_TOTAL_EXCEED_AMOUNT,
    FLAT_10_DISCOUNT_AMOUNT
} = require('../constants/discounts/flat10');
const {
    BULK_5_DISCOUNT,
    BULK_5_DISCOUNT_TOTAL_EXCEED_QUANTITY,
    BULK_5_DISCOUNT_PERCENTAGE
} = require('../constants/discounts/bulk5');
const {
    BULK_10_DISCOUNT,
    BULK_10_DISCOUNT_PERCENTAGE,
    BULK_10_DISCOUNT_TOTAL_EXCEED_QUANTITY
} = require('../constants/discounts/bulk10');
const {
    TIERED_50_DISCOUNT,
    TIERED_50_DISCOUNT_QUANTITY_THRESHOLD,
    TIERED_50_DISCOUNT_PERCENTAGE,
    TIERED_50_DISCOUNT_TOTAL_EXCEED_QUANTITY
} = require('../constants/discounts/tiered50');

function get_best_discount(products, total_amount) {

    discounts = {
        [FLAT_10_DISCOUNT]: 0,
        [BULK_5_DISCOUNT]: 0,
        [BULK_10_DISCOUNT]: 0,
        [TIERED_50_DISCOUNT]: 0
    };

    if (total_amount > FLAT_10_DISCOUNT_TOTAL_EXCEED_AMOUNT) {
        discounts[FLAT_10_DISCOUNT] = FLAT_10_DISCOUNT_AMOUNT;
    }

    products_exceed_bulk_5 = EMPTY_LIST;
    products_exceed_tiered_50 = EMPTY_LIST;
    total_quantity = 0;

    products.forEach((product) => {

        total_quantity += product.quantity;

        if (product.quantity > BULK_5_DISCOUNT_TOTAL_EXCEED_QUANTITY) {
            products_exceed_bulk_5.push(product);
        }

        if (product.quantity > TIERED_50_DISCOUNT_QUANTITY_THRESHOLD) {
            products_exceed_tiered_50.push(product);
        }

    });

    if (products_exceed_bulk_5.length > 0) {
        products_exceed_bulk_5.forEach((product) => {
            discounts[BULK_5_DISCOUNT] += (product.quantity * product.price * BULK_5_DISCOUNT_PERCENTAGE)
        });
    }

    if (total_quantity > BULK_10_DISCOUNT_TOTAL_EXCEED_QUANTITY) {
        discounts[BULK_10_DISCOUNT] = total_amount * BULK_10_DISCOUNT_PERCENTAGE;
    }

    if (total_quantity > TIERED_50_DISCOUNT_TOTAL_EXCEED_QUANTITY && products_exceed_tiered_50.length > 0) {
        products_exceed_tiered_50.forEach((product) => {
            discounts[TIERED_50_DISCOUNT] += ((product.quantity - TIERED_50_DISCOUNT_QUANTITY_THRESHOLD) * product.price * TIERED_50_DISCOUNT_PERCENTAGE);
        });
    }

    max_discount_name = EMPTY_STRING;
    max_discount_amount = 0;

    for (const discount in discounts) {
        if (discounts.hasOwnProperty(discount)) {
            const discountValue = discounts[discount];

            if (discountValue > max_discount_amount) {
                max_discount_name = discount;
                max_discount_amount = discountValue;
            }

        }
    }

    return {
        [DISCOUNT_NAME]: max_discount_name,
        [DISCOUNT_AMOUNT]: max_discount_amount
    }
};

module.exports = get_best_discount;