const Product = require('../models/product');
const { Product_A_NAME, Product_A_PRICE, Product_B_NAME, Product_B_PRICE, Product_C_NAME, Product_C_PRICE } = require('../constants/initialize.js');

function init_products() {
    return [
        new Product(Product_A_NAME, Product_A_PRICE),
        new Product(Product_B_NAME, Product_B_PRICE),
        new Product(Product_C_NAME, Product_C_PRICE)
    ];
}

module.exports = init_products;
