const { PRODUCT_QUANTITY_INPUT_MESSAGE, NEGATIVE_QUANTITY_ERROR_MESSAGE, NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE, GIFT_WRAPPER_INPUT_MESSAGE } = require('../constants/message');
const { TRUE_LOWERCASE, FALSE_LOWERCASE } = require('../constants/constant');
const InvalidGiftWrapperInputException = require('../exceptions/invalidGiftWrapper');
const InvalidInputForProductQuantity = require('../exceptions/invalidProductQuantity');
const readlineSync = require('readline-sync');


function get_products_input_user(products) {
    products.forEach((product) => {

        get_product_quantity_info(product);
        get_gift_wrapper_input(product);

    });

    return products;
};

function get_product_quantity_info(product) {
    try {
        quantity = readlineSync.questionInt(PRODUCT_QUANTITY_INPUT_MESSAGE(product.name, product.price));
        if (quantity < 0) {
            throw new InvalidInputForProductQuantity(NEGATIVE_QUANTITY_ERROR_MESSAGE);
        }

        product.quantity = quantity;
    } catch (error) {
        if (error instanceof InvalidInputForProductQuantity) {
            console.log(NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE);
            get_product_quantity_info(product);
        }
    }
};

function get_gift_wrapper_input(product) {
    wrapped = readlineSync.question(GIFT_WRAPPER_INPUT_MESSAGE);

    try {
        if (wrapped.toLowerCase() == TRUE_LOWERCASE) {
            product.isWrapped = true;
        } else if (wrapped.toLowerCase() == FALSE_LOWERCASE) {
            product.isWrapped = false;
        } else {
            throw new InvalidGiftWrapperInputException();
        }
    } catch (error) {
        if (error instanceof InvalidGiftWrapperInputException) {
            console.log(error.message);
            get_gift_wrapper_input(product);
        }
    }
};

module.exports = get_products_input_user;