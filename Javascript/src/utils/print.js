const { PRINT_PRODUCT_INFO_MESSAGE, PRINT_SUBTOTAL, PRINT_DISCOUNT_INFO, PRINT_SHIPPING_GIFT_AMOUNT, PRINT_TOTAL } = require('../constants/print');
const { DISCOUNT_NAME, DISCOUNT_AMOUNT, SHIPPING_AMOUNT, GIFT_AMOUNT } = require('../constants/dictionary');

function print_all_info(products, total_amount, discount_info, shipping_gift_amount) {
    print_product_info(products);
    print_subtotal(total_amount);
    print_discount_info(discount_info);
    print_shipping_gift_amount(shipping_gift_amount);
    print_total(total_amount, shipping_gift_amount, discount_info[DISCOUNT_AMOUNT]);
}

function print_product_info(products) {
    products.forEach((product) => {
        console.log(PRINT_PRODUCT_INFO_MESSAGE(product.name, product.quantity, (product.quantity * product.price)));
    });
};

function print_subtotal(total_amount) {
    console.log(PRINT_SUBTOTAL(total_amount));
};

function print_discount_info(discount) {
    console.log(discount);
    console.log(PRINT_DISCOUNT_INFO(discount[DISCOUNT_NAME], discount[DISCOUNT_AMOUNT]));
};

function print_shipping_gift_amount(amounts) {
    console.log(PRINT_SHIPPING_GIFT_AMOUNT(amounts[SHIPPING_AMOUNT], amounts[GIFT_AMOUNT]));
};

function print_total(total_amount, amounts, discount_amount) {
    total = total_amount + amounts[SHIPPING_AMOUNT] + amounts[GIFT_AMOUNT];
    console.log(PRINT_TOTAL(total - discount_amount));
};

module.exports = print_all_info;