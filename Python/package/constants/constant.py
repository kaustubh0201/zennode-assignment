# quantity for packaging
QUANTITY_PER_PACKAGE = 10
PRICE_PER_PACKAGE = 5

# for checking gift wrappers
TRUE_LOWERCASE = 'true'
FALSE_LOWERCASE = 'false'