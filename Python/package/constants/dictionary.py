# for discount information
DISCOUNT_NAME = "discount_name"
DISCOUNT_AMOUNT = "discount_amount"

# for sending shipping and gift amount
SHIPPING_AMOUNT = "shipping_amount"
GIFT_AMOUNT = "gift_amount"