# asking input to users string
GIFT_WRAPPER_INPUT_MESSAGE = "Do you want the gift wrapped ? (Please write True or False): \n"
PRODUCT_QUANTITY_INPUT_MESSAGE = "How much amount of {} is needed with each unit price as ${} ? \n"

# exception strings
NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE = "Only non-negative integers are accepted as input."
NEGATIVE_QUANTITY_ERROR_MESSAGE = "Got negative input for quantity."

# default exception message
INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE = "Got invalid input for gift wrapper."
INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE = "Got invalid input for product quantity."