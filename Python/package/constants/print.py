# sign for cash
DOLLAR = "$"

# print product info variables
PRODUCT_NAME = "Product Name: "
PRODUCT_QUANTITY = "Product Quantity: "
PRODUCT_TOTAL_AMOUNT = "Product Total Amount: "

# print sub total variables
SUB_TOTAL = "Sub Total: "

# print discount info variables
APPLIED_DISCOUNT_NAME = "Applied Discount Name: "
APPLIED_DISCOUNT_AMOUNT = "Applied Discount Amount: "

# print the miscellaneous charges variables
SHIPPING_FEE = "Shipping Fee: "
GIFT_WRAP_FEE = "Gift Wrap Fee: "

# print the total variable
TOTAL = "Total: "

# printing variables for showing to the user
PRINT_PRODUCT_INFO_MESSAGE = "\n\n" + PRODUCT_NAME + "{}\n" + PRODUCT_QUANTITY + "{}\n" + PRODUCT_TOTAL_AMOUNT + DOLLAR + "{}"
PRINT_SUBTOTAL = "\n\n" + SUB_TOTAL + DOLLAR + "{}"
PRINT_DISCOUNT_INFO = "\n\n" + APPLIED_DISCOUNT_NAME + "{}\n" + APPLIED_DISCOUNT_AMOUNT + DOLLAR + "{}"
PRINT_SHIPPING_GIFT_AMOUNT = "\n\n" + SHIPPING_FEE + DOLLAR + "{}\n" + GIFT_WRAP_FEE + DOLLAR + "{}"
PRINT_TOTAL = "\n\n" + TOTAL + DOLLAR + "{}"