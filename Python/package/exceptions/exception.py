from ..constants.message import INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE, INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE

class InvalidGiftWrapperInputException(Exception):
    def __init__(self, message = INVALID_GIFT_WRAPPER_DEFAULT_MESSAGE):
        self.message = message
        super().__init__(self.message)


class InvalidInputForProductQuantity(Exception):
    def __init__(self, message = INVALID_PRODUCT_QUANTITY_DEFAULT_MESSAGE):
        self.message = message
        super().__init__(self.message)