from .utils.amount import get_total_amount, get_shipping_gift_amount
from .utils.input import get_products_input_user
from .utils.initialize import init_products
from .utils.print import print_all_info
from .utils.discount import get_best_discount

def main():
    products = init_products()
    products = get_products_input_user(products)
    total_amount = get_total_amount(products)
    discount_info = get_best_discount(products, total_amount)
    shipping_gift_amount = get_shipping_gift_amount(products)
    print_all_info(products, total_amount, discount_info, shipping_gift_amount)

if __name__ == "__main__":
    main()