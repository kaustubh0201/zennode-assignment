class Product:
    def __init__(self, product_name, product_price):
        self.name = product_name
        self.price = product_price
        self.quantity = 0
        self.isWrapped = False