import math
from ..constants.dictionary import  SHIPPING_AMOUNT, GIFT_AMOUNT
from ..constants.constant import QUANTITY_PER_PACKAGE, PRICE_PER_PACKAGE

def get_total_amount(products):
    total_amount = 0
    
    for product in products:
        total_amount = total_amount + (product.quantity * product.price)

    return total_amount

def get_shipping_gift_amount(products):
    shipping_amount = 0
    gift_wrap_amount = 0

    for product in products:
        if (product.isWrapped == True):
            gift_wrap_amount += (product.quantity)

        shipping_amount += math.ceil(product.quantity / QUANTITY_PER_PACKAGE) * PRICE_PER_PACKAGE
    
    return {
        SHIPPING_AMOUNT: shipping_amount,
        GIFT_AMOUNT: gift_wrap_amount
    }