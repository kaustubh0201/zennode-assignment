from ..constants.empty import EMPTY_STRING, EMPTY_LIST
from ..constants.dictionary import DISCOUNT_NAME, DISCOUNT_AMOUNT
from ..constants.discounts.flat10 import FLAT_10_DISCOUNT, FLAT_10_DISCOUNT_TOTAL_EXCEED_AMOUNT, FLAT_10_DISCOUNT_AMOUNT
from ..constants.discounts.bulk5 import BULK_5_DISCOUNT, BULK_5_DISCOUNT_TOTAL_EXCEED_QUANTITY, BULK_5_DISCOUNT_PERCENTAGE
from ..constants.discounts.bulk10 import BULK_10_DISCOUNT, BULK_10_DISCOUNT_PERCENTAGE, BULK_10_DISCOUNT_TOTAL_EXCEED_QUANTITY
from ..constants.discounts.tiered50 import TIERED_50_DISCOUNT, TIERED_50_DISCOUNT_QUANTITY_THRESHOLD, TIERED_50_DISCOUNT_PERCENTAGE, TIERED_50_DISCOUNT_TOTAL_EXCEED_QUANTITY

def get_best_discount(products, total_amount):
    discounts = {
        FLAT_10_DISCOUNT: 0,
        BULK_5_DISCOUNT: 0,
        BULK_10_DISCOUNT: 0,
        TIERED_50_DISCOUNT: 0
    }

    if (total_amount > FLAT_10_DISCOUNT_TOTAL_EXCEED_AMOUNT):
        discounts[FLAT_10_DISCOUNT] = FLAT_10_DISCOUNT_AMOUNT

    products_exceed_bulk_5 = EMPTY_LIST
    products_exceed_tiered_50 = EMPTY_LIST
    total_quantity = 0

    for product in products:
        total_quantity = total_quantity + product.quantity
        
        if product.quantity > BULK_5_DISCOUNT_TOTAL_EXCEED_QUANTITY:
            products_exceed_bulk_5.append(product)

        if (product.quantity > TIERED_50_DISCOUNT_QUANTITY_THRESHOLD):
            products_exceed_tiered_50.append(product)

            

    if (len(products_exceed_bulk_5) > 0):
        for product in products_exceed_bulk_5:
            discounts[BULK_5_DISCOUNT] = discounts[BULK_5_DISCOUNT] + (product.quantity * product.price * BULK_5_DISCOUNT_PERCENTAGE)

    if (total_quantity > BULK_10_DISCOUNT_TOTAL_EXCEED_QUANTITY):
        discounts[BULK_10_DISCOUNT] = total_amount * BULK_10_DISCOUNT_PERCENTAGE

    if (total_quantity > TIERED_50_DISCOUNT_TOTAL_EXCEED_QUANTITY and len(products_exceed_tiered_50) > 0):
        for product in products_exceed_tiered_50:
            discounts[TIERED_50_DISCOUNT] = discounts[TIERED_50_DISCOUNT] + ((product.quantity - TIERED_50_DISCOUNT_QUANTITY_THRESHOLD) * product.price * TIERED_50_DISCOUNT_PERCENTAGE)

    
    max_discount_name = EMPTY_STRING
    max_discount_amount = 0

    for discount in discounts:
        if (discounts[discount] > max_discount_amount):
            max_discount_name = discount
            max_discount_amount = discounts[discount]

    return {
        DISCOUNT_NAME: max_discount_name,
        DISCOUNT_AMOUNT: max_discount_amount
    }

