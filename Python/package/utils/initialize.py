from ..models.model import Product
from ..constants.initialize import Product_A_NAME, Product_A_PRICE, Product_B_NAME, Product_B_PRICE, Product_C_NAME, Product_C_PRICE

def init_products():
    return [Product(Product_A_NAME, Product_A_PRICE), Product(Product_B_NAME, Product_B_PRICE), Product(Product_C_NAME, Product_C_PRICE)]