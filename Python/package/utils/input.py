from ..exceptions.exception import InvalidGiftWrapperInputException, InvalidInputForProductQuantity
from ..constants.message import PRODUCT_QUANTITY_INPUT_MESSAGE, NEGATIVE_QUANTITY_ERROR_MESSAGE, NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE, GIFT_WRAPPER_INPUT_MESSAGE
from ..constants.constant import TRUE_LOWERCASE, FALSE_LOWERCASE

def get_products_input_user(products):
    for product in products:
        get_product_quantity_info(product)
        get_gift_wrapper_input(product)

    return products

def get_product_quantity_info(product):
    try:
        quantity = int(input(PRODUCT_QUANTITY_INPUT_MESSAGE.format(product.name, product.price)))
        if (quantity < 0):
            raise InvalidInputForProductQuantity(NEGATIVE_QUANTITY_ERROR_MESSAGE)
        
        product.quantity = quantity
    except InvalidInputForProductQuantity as exception:
        print(exception.message)
        get_product_quantity_info(product)
    except ValueError as exception:
        print(NEGATIVE_QUANTITY_ERROR_PRINT_MESSAGE)
        get_product_quantity_info(product)

def get_gift_wrapper_input(product):
    wrapped = input(GIFT_WRAPPER_INPUT_MESSAGE)
        
    try:
        if (wrapped.lower() == TRUE_LOWERCASE):
            product.isWrapped = True
        elif (wrapped.lower() == FALSE_LOWERCASE):
            product.isWrapped = False
        else:
            raise InvalidGiftWrapperInputException()
    except InvalidGiftWrapperInputException as exception:
        print(exception.message)
        get_gift_wrapper_input(product)