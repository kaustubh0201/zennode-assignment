from ..constants.print import PRINT_PRODUCT_INFO_MESSAGE, PRINT_SUBTOTAL, PRINT_DISCOUNT_INFO, PRINT_SHIPPING_GIFT_AMOUNT, PRINT_TOTAL
from ..constants.dictionary import DISCOUNT_NAME, DISCOUNT_AMOUNT, SHIPPING_AMOUNT, GIFT_AMOUNT

def print_all_info(products, total_amount, discount_info, shipping_gift_amount):
    print_product_info(products)
    print_subtotal(total_amount)
    print_discount_info(discount_info)
    print_shipping_gift_amount(shipping_gift_amount)
    print_total(total_amount, shipping_gift_amount, discount_info[DISCOUNT_AMOUNT])

def print_product_info(products):
    for product in products:
        print(PRINT_PRODUCT_INFO_MESSAGE.format(product.name, product.quantity, (product.quantity * product.price)))

def print_subtotal(total_amount):
    print(PRINT_SUBTOTAL.format(total_amount))

def print_discount_info(discount):
    print(PRINT_DISCOUNT_INFO.format(discount[DISCOUNT_NAME], discount[DISCOUNT_AMOUNT]))

def print_shipping_gift_amount(amounts):
    print(PRINT_SHIPPING_GIFT_AMOUNT.format(amounts[SHIPPING_AMOUNT], amounts[GIFT_AMOUNT]))

def print_total(total_amount, amounts, discount_amount):
    total = total_amount + amounts[SHIPPING_AMOUNT] + amounts[GIFT_AMOUNT]
    print(PRINT_TOTAL.format(total - discount_amount))