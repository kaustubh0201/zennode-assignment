<!-- ABOUT THE PROJECT -->

# Zennode Assignment

The Task 1 and Task 2 given <a href="https://docs.google.com/document/d/16-JiaNH1qvQ-3OCd6wdR8TamYTO7r70e3UtEKKimN8M/edit">here</a> is done in this project.

The project has been done in Python and Javascript using Nodejs runtime environment. 

<hr>

## Task 1

<!-- GETTING STARTED -->

## Getting Started

Please follow the code snippets below to set up the environment for running the project on the local machine.

### Pre-requisite Installation

* <b>Python3</b>

  Python3 is essential for running the python project.

  ```sh
  > sudo apt update
  > sudo apt install python3
  > python3 --version
  ```
* <b>Nodejs</b>

  Nodejs and NPM is essential for running the javascript project.

  ```sh
  > sudo apt update
  > sudo apt install nodejs npm
  > node -v
  > npm -v
  ```

### Running the application

Ensure that the pre-requisite installations are done before coming to this step.

1. Clone the repository.
   ```sh
   git clone https://gitlab.com/kaustubh0201/zennode-assignment.git
   ```
2. Go to the Python folder for running the python project.
   ```sh
   cd Python
   ```
3. Run the application by directly calling Python3.
   ```sh
   python3 run.py
   ```
4. Run the application using some pre-written inputs.
   ```sh
   python3 run.py < ./testcases/input4
   ```
5. Go to the Javascript folder for running the javascript project.
   ```sh
   cd ../Javascript
   ```
6. First install a single dependency that makes life easier for taking input from user.
   ```sh
   npm install
   ```
7. Run the application by using nodejs runtime environment.
   ```sh
   node ./src/app.js
   ```

---

## Task 2

---

<b>Why do you like to join as a Software developer?</b>

<p align="justify">

1. Professional Growth: I am eager to embark on a career in software development and believe that this role provides a platform for continuous learning and professional development.

2. Hands-on Experience: The prospect of actively contributing to the design, implementation, and maintenance of software solutions is exciting.

3. Collaborative Environment: Zennode Technologies emphasizes collaboration with cross-functional teams, and I value the opportunity to work closely with colleagues to understand project requirements and deliver software solutions.

4. Emphasis on Best Practices: The company's commitment to coding best practices, code reviews, and maintaining clean, efficient code aligns with my belief in producing high-quality software.

5. Opportunity to Learn and Stay Updated: The job description highlights the importance of staying updated on industry trends and new technologies.

6. Motivation for Dynamic Environment: The mention of a dynamic environment resonates with my desire for a challenging and fast-paced work setting.   
</p>

<b>List top 3 softwares / tools that inspire you. Why do these tools inspire you?</b>

<p align="justify">

1. GitHub: GitHub is a collaborative platform that facilitates version control and collaborative software development. It inspires me because of its role in promoting open-source development, fostering collaboration among developers globally, and providing a centralized hub for code sharing and project management.

2. VS Code (Visual Studio Code): VS Code is a versatile and powerful source code editor that I find inspiring for its efficiency and extensibility. Its robust features, such as integrated debugging, intelligent code completion, and a vast library of extensions, enhance the development experience.

3. Jira: Jira is a project management and issue tracking tool that inspires me due to its effectiveness in streamlining development workflows. Its agile capabilities, customizable boards, and integration with development tools create a seamless project management experience.
</p>


<b>List top 3 organisations you like to join in your career & why you like these organisations.</b>

<p align="justify">

1. Google: Google is known for its innovative and cutting-edge work across various fields, including search, artificial intelligence, and cloud computing. The company is admired for its emphasis on creativity, a culture of continuous learning, and a commitment to making a positive impact on a global scale.

2. Microsoft: Microsoft is a technology giant with a long history of influencing the software industry. It is known for its diverse range of products and services, from operating systems to cloud computing.

3. Amazon: Amazon, particularly Amazon Web Services (AWS), has been a key player in the cloud computing industry. The company's customer-centric approach, focus on innovation, and commitment to long-term thinking make it an appealing choice for those interested in shaping the future of e-commerce, cloud services, and emerging technologies.

</p>

<b>How you position yourself in this industry after 2 years.</b>

<p align="justify">

   In the next two years, I envision myself as a seasoned and accomplished software developer, having honed my skills through hands-on experience and learning opportunities. I aim to contribute significantly to impactful projects, demonstrating proficiency in various programming languages and technologies.

   I aspire to take on more complex challenges, possibly specializing in a particular domain or technology stack. By staying updated on industry trends and emerging technologies, I aim to bring innovative solutions to the projects I work on.

   I plan to have an expanded skill set that may include expertise in web development technologies, proficiency in additional programming languages, and a deep understanding of agile methodologies.

   Overall, my goal is to be recognized not only for technical proficiency but also for my ability to collaborate effectively in a team, solve complex problems, and contribute to the success of the projects and organizations I am involved with.
</p>

<b>List 3 projects / products you have built or have planned to build in the upcoming year, also explain your role in the same.</b>

<p align="justify">

1. University Finder:

Developed a University Finder application that helps students discover and compare universities based on various criteria such as location, academic programs, and admission requirements. The platform aims to streamline the university selection process for prospective students.

As the software developer for the University Finder project, my responsibilities included collaborating with the team to design the application architecture, implementing core features, and ensuring the scalability and maintainability of the code. I actively participated in code reviews to maintain code quality.

2. Movie Booking Service:

Created a Movie Booking Service that allows users to browse movies, check showtimes, and book tickets seamlessly. The platform integrates with cinema databases to provide real-time information about movie schedules, seat availability, and ticket prices.
In my capacity as a software developer for the Movie Booking Service, I took on the responsibility of coding and implementing key features such as the booking engine, seat selection interface, and payment processing.

3. Job Tracking Application:

Currently in the planning stages, a Job Tracking Application is envisioned to help job seekers manage their job search activities effectively. The application will include features such as resume tracking, application status monitoring, and interview scheduling.
As the software developer for the Job Tracking Application project, my role involves defining the application architecture, selecting appropriate technologies, and implementing core functionalities. Collaboration with other developers and adherence to best practices will be essential throughout the development lifecycle.
</p>

---
